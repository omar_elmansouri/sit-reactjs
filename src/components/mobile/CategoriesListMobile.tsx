import React from 'react'
import RightIcon from '../../assets/icons/right-icon.png'
import { connect } from 'react-redux'
import { Category } from '../../redux/products/types'

interface Props {
  data?: Category[]
  onCategoryPress(category: Category): void
}

class CategoriesListMobile extends React.Component<Props> {
  render(): React.ReactNode {
    let categories = this.props.data || []
    const { onCategoryPress } = this.props
    return (
      <div className={`categories-list-mobile`}>
        <ul>
          {categories.map((category, index) => (
            <li key={index} onClick={() => onCategoryPress(category)} style={{backgroundImage: `url(data:${category.imageContentType};base64,${category.image})`}}>
              <p>
                {category.name}
              </p>
              <img src={RightIcon} alt="Right"/>
            </li>
          ))}
        </ul>
      </div>
    )
  }
}

const mapStateToProps = (state: any): any => {
  return {
    data: state.products.category
  }
}

export default connect(mapStateToProps, null)(CategoriesListMobile)
