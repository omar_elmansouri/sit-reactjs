import React from 'react'
import LogoImg from '../assets/images/logo.png'
import { Link } from 'react-router-dom'
import Cross from '../assets/icons/cross-white.png'

interface Props {
  onCartToggle?(): void
}

class AccountHeader extends React.Component<Props> {
  render(): React.ReactNode {
    return (
      <header id={'account-top-banner'}>
        <div className={'logo-container'}>
          <Link to={'/'}>
            <img src={LogoImg} className={'logo'} alt={'Logo image'} />
          </Link>
        </div>
        <Link to={'/'}>
          <img src={Cross} className={"imageIconCross"} />
        </Link>
        <div className={'nav-buttons'}>
          <Link to={'/'}>
            BACK TO HOME
          </Link>
        </div>
      </header>
    )
  }
}

export default AccountHeader
