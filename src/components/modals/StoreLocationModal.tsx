import React, { Component } from 'react'
import { WithTranslation } from 'react-i18next'
import CloseTag from '../CloseTag'

interface Props {
  onClose?(): void

  visible: boolean
}

class StoreLocationModal extends React.Component<Props> {
  render(): React.ReactNode {
    const {
      onClose,
      visible,
    } = this.props
    return (
      <div className={`modal ${visible ? 'open' : ''}`}>
        <div className={'body sm'}>
          <div className={'white-header ph'}>
            <CloseTag onClose={onClose}/>
            <h3>Store Location</h3>
          </div>
          <div className={'content'}>
            <iframe
              src={'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d54399.33880884685!2d74.30310837910154!3d31.55274870000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x391904c8f590d863%3A0xcbc3775cf0e3977a!2sCovo%20Restaurant!5e0!3m2!1sen!2s!4v1601211727006!5m2!1sen!2s'}
              width={320} frameBorder={0} style={{ border: 0 }} allowFullScreen={false}
              aria-hidden={false}
              tabIndex={0}/>
          </div>
        </div>
      </div>
    )
  }
}

export default StoreLocationModal
