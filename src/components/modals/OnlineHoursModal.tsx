import React from 'react'
import CloseTag from '../CloseTag'
import moment from 'moment'
import HorizontalLine from '../HorizontalLine'
import Option from '../../interfaces/Option'
import PlusIcon from '../../assets/icons/plus.png'
import MinusIcon from '../../assets/icons/minus.png'
import VerticalLine from '../VerticalLine'
import { CartItem } from '../../redux/cart/types'
import { Product } from '../../redux/products/types'
import SizeOptionCheckbox from '../SizeOptionCheckbox'

interface Props {
  onClose?(): void

  visible: boolean
}

interface State {
  option: string
}


class OnlineHoursModal extends React.Component<Props, State> {
  state = {
    option: 'PICKUP',
  }

  onOptionSelect = (option) => {
    this.setState({ option })
  }

  render(): React.ReactNode {
    const {
      onClose,
      visible,
    } = this.props
    return (
      <div className={`modal ${visible ? 'open' : ''}`}>
        <div className={'body  md'}>
          <div className={'header ph'}>
            <CloseTag onClose={onClose}/>
            <h3 className={'title'}>Online Hours</h3>
            <div className={'row'}>
              <p className={`option ${this.state.option === 'PICKUP' ? 'selected' : ''}`}
                 onClick={() => this.onOptionSelect('PICKUP')}>Pickup</p>
              <p className={`option ${this.state.option === 'DELIVERY' ? 'selected' : ''}`}
                 onClick={() => this.onOptionSelect('DELIVERY')}>Delivery</p>
            </div>
          </div>
          <div className={'content centered'}>
            {[...Array(7)].map((day, index) => moment().weekday(index).format('dddd')).map((day) =>
              (day !== moment().format('dddd')) ?
                <div className={'row'}><p className={'left-text'}>{day}</p><p>12:00am ~ 11:59pm</p></div> :
                <div className={'row'}><p className={'left-text bold'}>Today</p><p className={'bold'}>12:00am ~ 11:59pm</p></div>,
            )}
          </div>
        </div>
      </div>
    )
  }
}

export default OnlineHoursModal
