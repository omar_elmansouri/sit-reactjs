import React from 'react'
import TickIcon from '../../assets/icons/tick-white.png'

interface Props {
  visible: boolean
  close(): void
}

interface State {
}

class PasswordChangeModel extends React.Component<Props, State> {
  render(): React.ReactNode {
    const {
      visible,
    } = this.props
    return (
      <div className={`passwordChange-modal ${visible ? 'open' : ''}`}>
        <div className={'body'}>
          <div className={'green-icon'}>
            <img src={TickIcon} alt={'tick icon'}/>
          </div>
          <h2>Password Changed!</h2>
        </div>
      </div>
    )
  }
}

export default PasswordChangeModel
