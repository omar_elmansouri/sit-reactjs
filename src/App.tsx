import React from 'react';
import './assets/css/App.scss'
import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux'
import { store, persistor } from './redux/ConfigureStore'
import AppRouter from './AppRouter'

class App extends React.Component {
  render(): React.ReactNode {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
            <AppRouter/>
        </PersistGate>
      </Provider>
    )
  }
}

export default  App
