import axios from 'axios';

const Base_URL = "https://kwicksit.herokuapp.com/api/"

export function callOrderSubmit(action: any) {
    return axios.post(`${Base_URL}orders` , action.payload);
}
