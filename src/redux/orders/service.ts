import axios from 'axios'

const Base_URL = 'https://kwicksit.herokuapp.com/api'

const BASE_URL_DEV = 'https://kwicksit-dev.herokuapp.com/api'

export function callGetOrder(action: any) {
  return axios.get(`${Base_URL}/orders`, action.payload)
}

export function callCheckOutConfig(token: any) {
  return axios.get(`${BASE_URL_DEV}/checkout/config`, { headers: { 'Authorization': `Bearer ${token}` } })
}

export function callPaymentMethod(token: any) {
  return axios.post(`${BASE_URL_DEV}/checkout/payment-methods`, {}, { headers: { 'Authorization': `Bearer ${token}` } })
}

export function callInitiatePayment(body: any, ordersDTO: any, token: any) {
  return axios.post(`${BASE_URL_DEV}/checkout/initiate-payment`, {
    body,
    ordersDTO,
  }, { headers: { 'Authorization': `Bearer ${token}`, 'referer': 'https://kwicksit-dev.herokuapp.com/' } })
}
