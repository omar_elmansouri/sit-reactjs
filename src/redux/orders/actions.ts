import { types,LoadOrdersAction , ClickOrderRequest , ItemOption , ReClickOrderRequest} from './types'

export const LoadOrders = (): LoadOrdersAction => {
  return {
    type: types.LOAD_ORDERS_REQUEST,
  }
}

export const clickOrder = (order: ItemOption): ClickOrderRequest => {
  return {
    type: types.CLICK_ORDER_REQUEST,
    payload: order
  }
}
export const ReClickOrder = (order: ItemOption): ReClickOrderRequest => {
  return {
    type: types.CLICK_REORDER_REQUEST,
    payload: order
  }
}
export const checkOutConfigAction = (): any => {
  return {
    type: types.CHECK_OUT_CONFIG_REQUEST,
  }
}
export const paymentMethodAction = (): any => {
  return {
    type: types.GET_PAYMENT_METHODS_REQUEST,
  }
}
