import axios from 'axios';

const Base_URL = "https://kwicksit.herokuapp.com/api/"

export function callAllCategory(action: any) {
    return axios.get(`${Base_URL}one/stores/KFC`);
}
export function callAllProducts(action: any) {
    return axios.get(`${Base_URL}items?cacheBuster=1596615137284`);
}