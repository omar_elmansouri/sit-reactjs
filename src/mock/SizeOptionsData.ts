const SizeOptionsData = [
  { name: 'Regular', price: 20 },
  { name: 'Large', price: 25  },
]

export default SizeOptionsData
